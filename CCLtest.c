#ifndef F_CPU
#define F_CPU 1000000UL // 1MHz
#endif

#include <avr/io.h>

int main (void) {

/* Pin modes */

  PORTA.PIN1CTRL = PORT_PULLUPEN_bm; // Pullup on PA1 (push button)
  PORTA.DIRSET = PIN6_bm; // LED output on PA6

/* Setup CCL */

  CCL.TRUTH0 = 0x1;
  /* Truth table (pg383 of datasheet) is read bottom-to-top, 
  * in this case "0x1" == 00000001.
  * LED lights when LUT0-IN1 == low.
  * The inverse would be 0x4 (00000100) - LED always on until LUT0-IN1 == low.
  */
  CCL.LUT0CTRLB = CCL_ENABLE_bm | CCL_INSEL1_IO_gc; // PA1 (LUT0-IN1)
  CCL.LUT0CTRLA = CCL_ENABLE_bm | CCL_OUTEN_bm; // PA6 (LUT0-OUT)
  CCL.LUT0CTRLA |= CCL_FILTSEL_FILTER_gc; // Optional
  CCL.CTRLA = CCL_ENABLE_bm; // Enable CCL

  while (1) {
    /* Go to sleep */
    
    SLPCTRL.CTRLA = SLPCTRL_SEN_bm | SLPCTRL_SMODE_PDOWN_gc;
    // Enable and set pwr down mode
    __asm__ __volatile__ ( "SLEEP" "\n\t" :: ); // sleep_cpu() equiv
    SLPCTRL.CTRLA &= ~SLPCTRL_SEN_bm; // Disable sleep
    
    // Never runs, to confirm sleep mode:
    PORTA.OUTSET = PIN6_bm;
  }
  

}
