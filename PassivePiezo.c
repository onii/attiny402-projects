/* Working with a passive Piezo buzzer
*  Wiring:
*  PA6 -> 1k R -> Base of NPN
*  VCC -> 100 R & Flyback diode -> Piezo +
*  Piezo - -> Collector of NPN
*  Emitter of NPN -> GND
*/



// --- Clock speed ---
#ifndef F_CPU
#define F_CPU 32000UL // 32k
#endif

#include <avr/io.h>
#include <util/delay.h>

int main (void) {

  // --- Clock speed ---
  /* Set the Main clock to internal 32kHz oscillator*/
  _PROTECTED_WRITE(CLKCTRL.MCLKCTRLA, CLKCTRL_CLKSEL_OSCULP32K_gc);
  /* Set Main clock prescaler div to 2X and disable the Main clock prescaler */
  _PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, CLKCTRL_PDIV_2X_gc); // without enable
  /* ensure 20MHz isn't forced on*/
  _PROTECTED_WRITE(CLKCTRL.OSC20MCTRLA, 0x00);

  /* Pin modes */

  PORTA.DIRSET = PIN6_bm; // Piezo on PA6 (the only pin for TCB0 on 402)

  // CCMPL sets the period/freq
  TCB0.CCMPL = 3; /* PWM Period */

  // CCMPH sets the duty cycle
  TCB0.CCMPH = 3;   /* PWM Compare */

  TCB0.CTRLB = 0 << TCB_ASYNC_bp      /* Asynchronous Enable: disabled */
               | 1 << TCB_CCMPEN_bp   /* Pin Output Enable: enabled */
               | 0 << TCB_CCMPINIT_bp /* Pin Initial State: disabled */
               | TCB_CNTMODE_PWM8_gc; /* 8-bit PWM */

  TCB0.CTRLA = TCB_CLKSEL_CLKDIV2_gc  /* CLK_PER / 2 */
               | 0 << TCB_RUNSTDBY_bp /* Run Standby: disabled */
               | 0 << TCB_SYNCUPD_bp; /* Synchronize Update: disabled */
  
  while (1) {
    TCB0.CTRLA |= TCB_ENABLE_bm; // Enable
    _delay_ms(100);
    TCB0.CTRLA &= ~TCB_ENABLE_bm; // Disable
    _delay_ms(50);
    TCB0.CTRLA |= TCB_ENABLE_bm;
    _delay_ms(100);
    TCB0.CTRLA &= ~TCB_ENABLE_bm;
    _delay_ms(5000);
  }
  

}
