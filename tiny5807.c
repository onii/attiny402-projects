/* attiny402 + RDA5807M portable radio
* This is an attempt to recreate my arduino sketch
* in pure C, so I can use hardware features
* directly and save storage space as well.
* Written 4/22
*/

// --- Clock speed ---
#ifndef F_CPU
#define F_CPU 3333333
#endif

// --- Includes ---
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
//#include <avr/eeprom.h>
// ^ May require "-Wa,-mgcc-sir" avr-gcc flags

// --- Pin layout ---
//#define leftBtn PORTA.PIN7CTRL
//#define rightBtn PORTA.PIN6CTRL
//#define rightBtnInt PORT_INT6_bm



// --- Constants ---

// stuff

// --- Global vars ---
volatile uint8_t btnPushed = 0;

// --- Interrupts ---
ISR(PORTA_PORT_vect) {               // Pin interrupt ISR
  PORTA.INTFLAGS = PORT_INT7_bm;       // Clear flag by writing "1" to position
  PORTA.PIN7CTRL |= PORT_ISC_INTDISABLE_gc; // Disable pin ISR
  btnPushed = 1;
}

ISR(RTC_PIT_vect) {            // RTC interrupt ISR
  RTC.PITINTFLAGS = RTC_PI_bm; // Clear flag by writing "1" to position
}

// --- Functions ---
uint8_t goToSleep(void) {
  SLPCTRL.CTRLA = SLPCTRL_SEN_bm | SLPCTRL_SMODE_PDOWN_gc;
  // Enable and set pwr down mode
  __asm__ __volatile__ ( "sleep" "\n\t" :: ); // sleep_cpu() equiv
  // ^ Copied from: https://hackaday.io/project/165881
  SLPCTRL.CTRLA &= ~SLPCTRL_SEN_bm; // Disable sleep
  return 0;
}

// --- Main ---

int main(void) {
  PORTA.PIN7CTRL = PORT_PULLUPEN_bm;
  PORTA.PIN6CTRL = PORT_PULLUPEN_bm;
  // PA1 and PA2 for TWI (not sure what to do here)

  PORTA.DIRSET = PIN3_bm;
  sei();
  while (1) {
    if (btnPushed == 1) {
      btnPushed = 0;
      for (uint8_t i = 0; i <= 5; i++) {
        _delay_ms(500);
        PORTA.OUTSET = PIN3_bm;
        _delay_ms(500);
        PORTA.OUTCLR = PIN3_bm;
      }
    }
    goToSleep();
  }
}
